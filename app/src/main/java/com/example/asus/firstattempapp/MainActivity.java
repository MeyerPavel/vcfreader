package com.example.asus.firstattempapp;

import android.content.Context;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.Manifest;

import com.example.asus.firstattempapp.FileExplorer_Java.MainActivity_FileExplorer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_ALL_PERMISSONS = 1;

    ContentResolver contentResolver;
    FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contentResolver = getContentResolver();
        String[] Permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS, };
        if(!hasPermissions(this, Permissions)){
            ActivityCompat.requestPermissions(this, Permissions, REQUEST_CODE_ALL_PERMISSONS);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions){
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M && context!=null && permissions!=null){
            for(String permission: permissions){
                if(ActivityCompat.checkSelfPermission(context, permission)!=PackageManager.PERMISSION_GRANTED){
                    return  false;
                }
            }
        }
        return true;
    }

    public void UserDirectorySearch(View view) {
        Intent intent = new Intent(this, MainActivity_FileExplorer.class);
        startActivity(intent);
    }


    public void RecursionFindFile(View view) throws ExecutionException, InterruptedException {

        FindVCFTask fvt = new FindVCFTask();
        fvt.execute();
        HashMap<String,String> hmap = fvt.get();
        ArrayList<String> list = new ArrayList<>();
        list.addAll(hmap.keySet());
        ListViewFragment listFragment = new ListViewFragment();
        listFragment.setData(list,hmap);
        transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frgmCont,listFragment);
        transaction.commit();
    }
    public class FindVCFTask extends AsyncTask<Void,Void,HashMap<String,String>> {

        ProgressBarFragment barFragment;
        FragmentTransaction transaction;
        HashMap<String,String> hmapFileNameAndPath = new HashMap<>();

        @Override
        protected HashMap<String, String> doInBackground(Void... voids) {
            Filewalker fw = new Filewalker();
            fw.walk(Environment.getExternalStorageDirectory().toString());
            hmapFileNameAndPath.putAll(fw.hmapFileNameAndPath);
            return hmapFileNameAndPath;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            barFragment = new ProgressBarFragment();
            transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.frgmCont, barFragment);
            transaction.commit();
        }

        @Override
        protected void onPostExecute(HashMap<String,String> _hashMapFileNameandPath) {
            super.onPostExecute(_hashMapFileNameandPath);
        }
    }
}

