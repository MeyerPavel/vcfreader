package com.example.asus.firstattempapp;

import android.util.Log;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ASUS on 18.05.2018.
 */

class FindCertainExtension  {


    public void listFile(File dir, String ext, HashMap<String,String> hmapFileNameandPath) {

        GenericExtFilter filter = new GenericExtFilter(ext);

        String[] list = dir.list(filter);
        try{
            if (list.length == 0) {
                //System.out.println("no files end with : " + ext);
                return;
            }
        }catch(NullPointerException exc){
            return;
        }
        for (String fileName : list) {
            String fullPath = new StringBuilder(dir.getAbsolutePath()).append(File.separator)
                    .append(fileName).toString();
            Log.v(fileName,fullPath);
            hmapFileNameandPath.put(fileName,fullPath);
        }
    }

    // inner class, generic extension filter
    public class GenericExtFilter implements FilenameFilter {

        private String ext;

        private GenericExtFilter(String ext) {
            this.ext = ext;
        }

        @Override
        public boolean accept(File dir, String name) {
            return (name.endsWith(ext));
        }
    }
}
