package com.example.asus.firstattempapp;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by ASUS on 06.05.2018.
 */

public class ContactsLoader {

    private String filePath;

    private StringBuilder Line;
    private ContentResolver contentResolver;

    ContactsLoader(ContentResolver _contentResolver,String _filePath){
        contentResolver = _contentResolver;
        filePath = _filePath;
    }

    public void addContact() throws FileNotFoundException, IOException {

        File file = new File(filePath);//Meyer.vcf
        try (Scanner scanner = new Scanner(file)) {
            StringBuilder subStr;
            String lineAfterFN;
            ArrayList<ContentProviderOperation> op = new ArrayList<>();
            while (scanner.hasNextLine()) {
                Line = new StringBuilder(scanner.nextLine());
                try {
                    subStr = new StringBuilder(KeyWordSearch());
                    if (subStr.toString().equals("FN")) {
                        try {
                            contentResolver.applyBatch(ContactsContract.AUTHORITY, op);
                        } catch (Exception e) {

                            Log.e("Exception: ", e.getMessage());
                        }
                        op.clear();
                        op.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                                .build());
                        lineAfterFN = scanner.nextLine();
                        printToFileByKeywords(subStr.toString(), lineAfterFN,op);
                    } else {
                        printToFileByKeywords(subStr.toString(),op);
                    }

                } catch (Exception ignored) {
                }
            }
        }
    }


    private String KeyWordSearch() {
        int indexOfSemicolon = Line.indexOf(";");
        int indexOfColon = Line.indexOf(":");
        if ((indexOfSemicolon < indexOfColon) && indexOfSemicolon > 0) {
            return Line.substring(0, indexOfSemicolon);
        } else if ((indexOfSemicolon > indexOfColon) && indexOfColon > 0) {
            return Line.substring(0, indexOfColon);
        } else if (indexOfColon < 0 && indexOfSemicolon > 0) {
            return Line.substring(0, indexOfSemicolon);
        } else if (indexOfSemicolon < 0 && indexOfColon > 0) {
            return Line.substring(0, indexOfColon);
        } else {
            return "";
        }
    }

    private void printToFileByKeywords(String str,ArrayList<ContentProviderOperation> op) throws IOException {
        int indexOfColon = 0;
        String Contact_Number;
        if (str.equals("TEL")) {
            indexOfColon = Line.indexOf(":");
            if (isPrefTel(Line.toString())) {
                Contact_Number = Line.substring(indexOfColon + 1, Line.length()).replace(";", " ");
                contentProviderInsert_PrefNumber(Contact_Number,op);
            } else {
                Contact_Number = Line.substring(indexOfColon + 1, Line.length()).replace(";", " ");
                contentProviderInsert_ContactNumber(Contact_Number,op);
            }
        }

    }

    private void printToFileByKeywords( String str, String strLine,ArrayList<ContentProviderOperation> op) throws IOException {
        int indexOfColon = Line.indexOf(":");
        byte[] arrb;
        String Contact_Name;
        String Contact_Number;
        if (isContinuationOfFN(strLine)) {
            String temp = Line.substring(indexOfColon + 1, Line.length() - 1) + strLine;
            arrb = temp.getBytes(Charset.forName("UTF-8"));
            Contact_Name = QuotedPrintableDecoder.decode(arrb, temp);
            contentProviderInsert_ContactName(Contact_Name,op);

        } else {
            String temp = Line.substring(indexOfColon + 1, Line.length());
            arrb = temp.getBytes(Charset.forName("UTF-8"));
            if (!Line.toString().contains("=")) {
                Contact_Name = Line.substring(indexOfColon + 1, Line.length()).replace(";", " ");
                contentProviderInsert_ContactName(Contact_Name,op);
            }
            else
                Contact_Name = QuotedPrintableDecoder.decode(arrb, temp);
            contentProviderInsert_ContactName(Contact_Name,op);
        }
        indexOfColon = strLine.indexOf(":");
        if (!isContinuationOfFN(strLine)) {
            if (isPrefTel(strLine)) {
                Contact_Number = strLine.substring(indexOfColon + 1, strLine.length()).replace(";", " ");
                contentProviderInsert_PrefNumber(Contact_Number,op);
            } else {
                Contact_Number = strLine.substring(indexOfColon + 1, strLine.length()).replace(";", " ");
                contentProviderInsert_ContactNumber(Contact_Number,op);
            }
        }
    }

    private boolean isContinuationOfFN(String str) {
        for (String splitStr : str.split("[;:/]")) {
            if (splitStr.equals("TEL"))
                return false;
        }
        return true;

    }
    private boolean isPrefTel(String str) {
        for (String splitStr : str.split("[;:]")) {
            if (splitStr.equals("PREF"))
                return true;
        }
        return false;
    }

    private void contentProviderInsert_ContactName(String Contact_Name,ArrayList<ContentProviderOperation> op){
        op.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, Contact_Name)
                .build());
    }

    private void contentProviderInsert_ContactNumber(String Contact_Number,ArrayList<ContentProviderOperation> op){
        op.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, Contact_Number)
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                .build());
    }

    private void contentProviderInsert_PrefNumber(String Contact_Number,ArrayList<ContentProviderOperation> op){
        op.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, Contact_Number)
                .withValue(ContactsContract.CommonDataKinds.Phone.IS_SUPER_PRIMARY, 1)
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                .build());
    }
}
