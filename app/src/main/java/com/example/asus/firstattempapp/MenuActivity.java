package com.example.asus.firstattempapp;

import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class MenuActivity extends AppCompatActivity {

    ContentResolver contentResolver;
    String fileName;
    String filePath;
    ProgressBar progressBar;

    final String OPERATION_ADDCONTACTS_CODE = "ADDContacts";
    final String OPERATION_SETTING_DEFAULT_CODE = "SET_DEFAULT";
    final String OPERATION_ADD_MISSED_NUMBERS_CODE = "ADD_MISSED";
    final String OPERATION_WRITE_TO_TXT_CODE = "WRITE_TXT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        contentResolver = getContentResolver();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        Intent intent = getIntent();
        fileName = intent.getStringExtra("FileName");
        filePath = intent.getStringExtra("FilePath");
        Log.i("FILEPATH","====================================" + filePath);
    }

    public void addContact(View view) throws FileNotFoundException, IOException, ExecutionException, InterruptedException {
        WaitAsyncTask waitAsyncTask = new WaitAsyncTask();
        waitAsyncTask.execute(OPERATION_ADDCONTACTS_CODE);
    }

    public void SettingDefaultNumbers(View view) throws FileNotFoundException {
        WaitAsyncTask waitAsyncTask = new WaitAsyncTask();
        waitAsyncTask.execute(OPERATION_SETTING_DEFAULT_CODE);
    }

    public void AddMissedPhoneNumbers(View view) throws FileNotFoundException {
        WaitAsyncTask waitAsyncTask = new WaitAsyncTask();
        waitAsyncTask.execute(OPERATION_ADD_MISSED_NUMBERS_CODE);
    }

    public void WriteToTXT(View view) throws IOException {
        WaitAsyncTask waitAsyncTask = new WaitAsyncTask();
        waitAsyncTask.execute(OPERATION_WRITE_TO_TXT_CODE);
    }

    public class WaitAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String...OPERATION_CODE) {

            try {
                switch (OPERATION_CODE[0]){
                    case OPERATION_ADDCONTACTS_CODE : {
                        ContactsLoader contactsLoader = new ContactsLoader(contentResolver,filePath);
                        contactsLoader.addContact();
                        break;
                    }
                    case OPERATION_SETTING_DEFAULT_CODE :{
                        NumberLoader numberLoader = new NumberLoader(contentResolver,filePath);
                        numberLoader.settingTheCorrectPrefNumbers();
                        break;
                    }
                    case OPERATION_ADD_MISSED_NUMBERS_CODE :{
                        NumberLoader numberLoader = new NumberLoader(contentResolver,filePath);
                        numberLoader.addNonPrefNumbers();
                        break;
                    }
                    case OPERATION_WRITE_TO_TXT_CODE :{
                        WriteFromVCTtoTXT writeFromVCTtoTXT = new WriteFromVCTtoTXT(filePath);
                        writeFromVCTtoTXT.WritetoTXT();
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }


        @Override
        protected void onPreExecute() {;
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }


        @Override
        protected void onPostExecute(String _hashMapFileNameandPath) {
            super.onPostExecute(_hashMapFileNameandPath);
            progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(),"Complited",Toast.LENGTH_LONG).show();
        }
    }
}
