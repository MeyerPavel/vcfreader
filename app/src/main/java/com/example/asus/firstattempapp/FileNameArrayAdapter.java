package com.example.asus.firstattempapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 30.06.2018.
 */

public class FileNameArrayAdapter extends ArrayAdapter<String> {
    private Context mContext; //Activity context.
    private int mResource; //Represents the list_rowl file (our rows) as an int e.g. R.layout.list_row
    private  ArrayList<String> mObjects; //The List of objects we got from our model.

    public FileNameArrayAdapter(Context c, int res,  ArrayList<String> o) {
        super(c, res, o);
        mContext = c;
        mResource = res;
        mObjects = o;
    }

    public FileNameArrayAdapter(Context c, int res) {
        super(c, res);
        mContext = c;
        mResource = res;
    }

    /*Does exactly what it looks like.  Pulls out a specific File Object at a specified index.
    Remember that our FileArrayAdapter contains a list of Files it gets from our model's getAllFiles(),
    so getitem(0) is the first file in that List, getItem(1), the second, etc.  ListView uses this
    method internally.*/
/*    @Override
    public String getItem(int i) {
        return mObjects.get(i);
    }

    *//** Allows me to pull out specific views from the row xml file for the ListView.   I can then
     *make any modifications I want to the ImageView and TextViews inside it.
     *@param position - The position of an item in the List received from my model.
     *@param convertView - list_row.xml as a View object.
     *@param parent - The parent ViewGroup that holds the rows.  In this case, the ListView.
     ***/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        /*This is the entire file [list_rowl] with its RelativeLayout, ImageView, and two
        TextViews.  It will always be null the very first time, so we need to inflate it with a
           LayoutInflater.*/
        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater.from(mContext));

            v = inflater.inflate(mResource, null);
        }

        /* We pull out the ImageView and TextViews so we can set their properties.*/
        ImageView iv = (ImageView) v.findViewById(R.id.fileImageView);
        TextView nameView = (TextView) v.findViewById(R.id.file_name_textview);
        iv.setImageResource(R.drawable.fileimage);

        String file_Name = mObjects.get(position);

        nameView.setText(file_Name);

        //Send the view back so the ListView can show it as a row, the way we modified it.
        return v;
    }
}
