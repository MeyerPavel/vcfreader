package com.example.asus.firstattempapp;

import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by ASUS on 05.05.2018.
 */

public class WriteFromVCTtoTXT {

    private String filePath;
    private final String FULL_NAME = "Full Name";
    private final String TEL = "TEL";
    private StringBuilder Line;

    WriteFromVCTtoTXT(String _filePath){
        filePath = _filePath;
    }

    public void WritetoTXT() throws IOException {
        File file = new File(filePath);//Meyer.vcf
        try (Scanner scanner = new Scanner(file); FileWriter writer = new FileWriter(filePath.replace(filePath.substring(filePath.lastIndexOf(".")+1),"txt"))) {
            StringBuilder subStr;
            String lineAfterFN;
            while(scanner.hasNextLine()){

                Line = new StringBuilder(scanner.nextLine());
                try{
                    subStr = new StringBuilder(KeyWordSearch());
                    if(subStr.toString().equals("FN")){
                        writer.write("\r\n");
                        lineAfterFN = scanner.nextLine();
                        printToFileByKeywords(writer,subStr.toString(), lineAfterFN);
                    }
                    else{
                        printToFileByKeywords(writer,subStr.toString());
                    }
                }catch(Exception ignored){
                }

            }

        }

    }
    private String KeyWordSearch(){
        int indexOfSemicolon = Line.indexOf(";");
        int indexOfColon = Line.indexOf(":");
        if((indexOfSemicolon < indexOfColon) && indexOfSemicolon > 0){
            return Line.substring(0, indexOfSemicolon);
        }
        else if((indexOfSemicolon > indexOfColon) && indexOfColon > 0){
            return Line.substring(0, indexOfColon);
        }
        else if(indexOfColon < 0 && indexOfSemicolon > 0){
            return Line.substring(0, indexOfSemicolon);
        }
        else if(indexOfSemicolon < 0 && indexOfColon > 0){
            return Line.substring(0, indexOfColon);
        }
        else{
            return "";
        }
    }
    private void printToFileByKeywords(FileWriter printToFile,String str) throws IOException{
        int indexOfColon = 0;
        if(str.equals("TEL")){
            indexOfColon = Line.indexOf(":");
            if(isPrefTel(Line.toString())){
                printToFile.write(TEL + ">" + "DEFAULT>|" + Line.substring(indexOfColon+1,Line.length()).replace(";", " ") + "\r\n");
            }
            else{
                printToFile.write(TEL + ">|" + Line.substring(indexOfColon+1,Line.length()).replace(";", " ") + "\r\n");
            }
        }

    }
    private void printToFileByKeywords(FileWriter printToFile,String str,String strLine) throws IOException{
        int indexOfColon = Line.indexOf(":");
        byte[] arrb;
        if(isContinuationOfFN(strLine)){
            String temp = Line.substring(indexOfColon+1,Line.length() - 1) + strLine;
            arrb = temp.getBytes(Charset.forName("UTF-8"));
            if(!Line.toString().contains("="))
                printToFile.write(FULL_NAME + ">|" + temp + "\r\n");
            else
                printToFile.write(FULL_NAME + ">|" + QuotedPrintableDecoder.decode(arrb,temp) + "\r\n");

        }
        else{
            String temp = Line.substring(indexOfColon+1,Line.length());
            arrb = temp.getBytes(Charset.forName("UTF-8"));
            if(!Line.toString().contains("="))
                printToFile.write(FULL_NAME + ">|" + Line.substring(indexOfColon+1,Line.length()).replace(";", " ") + "\r\n");
            else
                printToFile.write(FULL_NAME + ">|" + QuotedPrintableDecoder.decode(arrb,temp) + "\r\n");
        }
        indexOfColon = strLine.indexOf(":");
        if(!isContinuationOfFN(strLine)){
            if(isPrefTel(strLine)){
                printToFile.write(TEL + ">" + "DEFAULT>|" + strLine.substring(indexOfColon+1,strLine.length()).replace(";", " ") + "\r\n");
            }
            else{
                printToFile.write(TEL + ">|" + strLine.substring(indexOfColon+1,strLine.length()).replace(";", " ") + "\r\n");
            }
        }
    }

    private boolean isContinuationOfFN(String str){
        for(String splitStr : str.split("[;:/]")){
            if(splitStr.equals("TEL"))
                return false;
        }
        return true;
    }
    private boolean isPrefTel(String str){
        for(String splitStr : str.split("[;:]")){
            if(splitStr.equals("PREF"))
                return true;
        }
        return false;
    }
}
