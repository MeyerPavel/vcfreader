package com.example.asus.firstattempapp;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;




class NumberLoader {

    private ContentResolver contentResolver;
    private String filePath;

    private Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
    private String _ID = ContactsContract.Contacts._ID;
    private String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
    private String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
    private String IS_PRIMARY = ContactsContract.Data.IS_PRIMARY;

    private Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
    private String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
    private String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;


    NumberLoader(ContentResolver _contentResolver,String _filePath){
        filePath = _filePath;
        contentResolver = _contentResolver;
    }

    public void addNonPrefNumbers() throws FileNotFoundException {
        initializationContactFields();

        String phoneNumber;
        String prefNumber = "";
        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);
        ArrayList<String> phoneNumbersList = new ArrayList<>();
        ////
        HashMapContactsData hashMapContactsData = new HashMapContactsData(filePath);
        HashMap<String, ArrayList<String>> hmapData = new HashMap<>();
        HashMap<String, String> hmap_prefNumber  = new HashMap<>();
        hashMapContactsData.assign_HashMapLink(hmapData, hmap_prefNumber);
        for(String t : hmapData.keySet()) {
            Log.i("KEYSET","------------" + t);
        }
        //Запускаем цикл обработчик для каждого контакта:
        assert cursor != null;
        if (cursor.getCount() > 0) {

            //Если значение имени и номера контакта больше 0 (то есть они существуют) выбираем
            //их значения в приложение привязываем с соответствующие поля "Имя" и "Номер":
            while (cursor.moveToNext()) {
                ////
                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));

                    if (hasPhoneNumber > 0 && (hmapData.containsKey(name)|| hmap_prefNumber.containsKey(name))) {
                        Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null,
                                Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
                        assert phoneCursor != null;
                        while (phoneCursor.moveToNext()) {
                            int value = Integer.parseInt(phoneCursor.getString(phoneCursor.getColumnIndex(IS_PRIMARY)));
                            phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                            Log.i("PhoneNumber",phoneNumber);
                            if (value != 0) {
                                prefNumber = (hmap_prefNumber.get(name) != null) ? hmap_prefNumber.get(name) : phoneNumber;
                                if (hmap_prefNumber.get(name) != null)
                                    phoneNumbersList.add(phoneNumber);
                            } else {
                                phoneNumbersList.add(phoneNumber);
                            }
                        }

                        if (prefNumber.length() == 0 && hmap_prefNumber.get(name) != null) {
                            prefNumber = hmap_prefNumber.get(name);
                        }
                        //////

                        addMissingNumbersFromHashMap(phoneNumbersList, hmapData.get(name));
                        /////
                        phoneNumbersList.remove(prefNumber);
                        updateContactPhoneByName(name, phoneNumbersList, prefNumber);
                        prefNumber = "";
                        phoneNumbersList.clear();
                    }
            }
        }
    }

    public void settingTheCorrectPrefNumbers() throws FileNotFoundException {
        initializationContactFields();


        String phoneNumber;
        String prefNumber = "";
        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);
        ArrayList<String> phoneNumbersList = new ArrayList<>();
        ////

        HashMapContactsData hashMapContactsData = new HashMapContactsData(filePath);
        HashMap<String, ArrayList<String>> hmapData = new HashMap<>();
        HashMap<String, String> hmap_prefNumber  = new HashMap<>();
        hashMapContactsData.assign_HashMapLink(hmapData, hmap_prefNumber);
        ////
        //Запускаем цикл обработчик для каждого контакта:
        assert cursor != null;
        if (cursor.getCount() > 0) {

            //Если значение имени и номера контакта больше 0 (то есть они существуют) выбираем
            //их значения в приложение привязываем с соответствующие поля "Имя" и "Номер":
            while (cursor.moveToNext()) {
                ////

                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));

                if (hasPhoneNumber > 0 && (hmapData.containsKey(name)|| hmap_prefNumber.containsKey(name))) {
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null,
                            Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
                    assert phoneCursor != null;
                    while (phoneCursor.moveToNext()) {
                        int value = Integer.parseInt(phoneCursor.getString(phoneCursor.getColumnIndex(IS_PRIMARY)));
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        if (value != 0) {
                            prefNumber = (hmap_prefNumber.get(name) != null) ? hmap_prefNumber.get(name) : phoneNumber;
                            if(hmap_prefNumber.get(name) != null) phoneNumbersList.add(phoneNumber);
                        } else {
                            phoneNumbersList.add(phoneNumber);
                        }
                    }

                    if (prefNumber.length() == 0 && hmap_prefNumber.get(name) != null) {
                        prefNumber = hmap_prefNumber.get(name);
                    }
                    phoneNumbersList.remove(prefNumber);
                    updateContactPhoneByName(name, phoneNumbersList, prefNumber);
                    prefNumber = "";
                    phoneNumbersList.clear();

                }
            }
        }
    }

    private void initializationContactFields(){
        CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
         _ID = ContactsContract.Contacts._ID;
        DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        IS_PRIMARY = ContactsContract.Data.IS_PRIMARY;

        PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

    }
    private void addMissingNumbersFromHashMap(ArrayList<String> _phoneNumbersList,ArrayList<String> hmapValues){
        boolean flag = false;
        for(String hmapNumber :hmapValues){
            for(String phoneList : _phoneNumbersList){
                if(hmapNumber.equals(phoneList))
                    flag = true;
            }
            if(!flag) _phoneNumbersList.add(hmapNumber);
            flag = false;
        }

    }

    private long getRawContactIdByName(String displayName)
    {
        //ContentResolver contentResolver = getContentResolver();

        // Query raw_contacts table by display name field ( given_name family_name ) to get raw contact id.

        // Create query column array.
        String queryColumnArr[] = {ContactsContract.RawContacts._ID};

        // Create where condition clause.
        String whereClause = ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY + " = '" + displayName + "'";

        // Query raw contact id through RawContacts uri.
        Uri rawContactUri = ContactsContract.RawContacts.CONTENT_URI;

        // Return the query cursor.
        Cursor cursor = contentResolver.query(rawContactUri, queryColumnArr, whereClause, null, null);

        long rawContactId = -1;

        if(cursor!=null)
        {
            // Get contact count that has same display name, generally it should be one.
            int queryResultCount = cursor.getCount();
            // This check is used to avoid cursor index out of bounds exception. android.database.CursorIndexOutOfBoundsException
            if(queryResultCount > 0)
            {
                // Move to the first row in the result cursor.
                cursor.moveToFirst();
                // Get raw_contact_id.
                rawContactId = cursor.getLong(cursor.getColumnIndex(ContactsContract.RawContacts._ID));
            }
        }

        return rawContactId;
    }
    /*
 * Update contact phone number by contact name.
 * Return update contact number, commonly there should has one contact be updated.
 */
    private int updateContactPhoneByName(String displayName,ArrayList<String> _phoneNumbers,String _prefNumber)
    {

        int ret = 0;

        //ContentResolver contentResolver = getContentResolver();

        // Get raw contact id by display name.
        long rawContactId = getRawContactIdByName(displayName);

        // Update fileNames table phone number use contact raw contact id.
        if(rawContactId > -1) {
            // Update mobile phone number.
            updatePhoneNumber(contentResolver, rawContactId, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE, _phoneNumbers,_prefNumber);
            ret = 1;
        }else
        {
            ret = 0;
        }

        return ret;
    }

    /* Update phone number with raw contact id and phone type.*/
    private void updatePhoneNumber(ContentResolver contentResolver, long rawContactId, int phoneType, ArrayList<String> newPhoneNumbers, String prefNumber) {
        Uri dataUri = ContactsContract.Data.CONTENT_URI;
        String mimetype = ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE;
        String whereClauseBuf = ContactsContract.Data.RAW_CONTACT_ID +
                "=" +
                rawContactId +
                " and " +
                ContactsContract.Data.MIMETYPE +
                " = '" +
                mimetype +
                "'" +
                " and " +
                ContactsContract.CommonDataKinds.Phone.TYPE +
                " = " +
                phoneType;

        // Specify the update contact id.

        // Specify the row fileNames mimetype to phone mimetype( vnd.android.cursor.item/phone_v2 )

        // Specify phone type.

        int deleteNumbers = contentResolver.delete(dataUri, whereClauseBuf, null);

        ContentValues contentValues = new ContentValues();

        for (int count = 0; count < newPhoneNumbers.size(); count++) {
            contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
            contentValues.put(ContactsContract.Data.MIMETYPE, mimetype);
            contentValues.put(ContactsContract.CommonDataKinds.Phone.TYPE, phoneType);
            contentValues.put(ContactsContract.CommonDataKinds.Phone.NUMBER, newPhoneNumbers.get(count));
            Uri addDataCont = contentResolver.insert(dataUri, contentValues);
        }
        if(!prefNumber.equals("")) {
            contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
            contentValues.put(ContactsContract.Data.MIMETYPE, mimetype);
            contentValues.put(ContactsContract.CommonDataKinds.Phone.TYPE, phoneType);
            contentValues.put(ContactsContract.CommonDataKinds.Phone.NUMBER, prefNumber);
            contentValues.put(ContactsContract.CommonDataKinds.Phone.IS_SUPER_PRIMARY, 1);
            Uri addDataCont = contentResolver.insert(dataUri, contentValues);
        }
    }

}
