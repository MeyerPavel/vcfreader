package com.example.asus.firstattempapp;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ASUS on 13.05.2018.
 */

public class Filewalker {

    public HashMap<String,String> hmapFileNameAndPath = new HashMap<>();

    private static final String FILE_TEXT_EXT = ".vcf";


    public void walk( String path ) {
        File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;
        for ( File f : list) {
            if (f.isDirectory()) {
                new FindCertainExtension().listFile(f, FILE_TEXT_EXT,hmapFileNameAndPath);
                walk(f.getAbsolutePath());
            }
        }
    }
}
