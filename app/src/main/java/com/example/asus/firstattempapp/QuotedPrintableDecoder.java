package com.example.asus.firstattempapp;

import java.io.UnsupportedEncodingException;
/**
 * Created by ASUS on 10.03.2018.
 */

public class QuotedPrintableDecoder { private static byte HT = 0x09; // \t
    private static byte LF = 0x0A; // \n
    private static byte CR = 0x0D; // \r

    public static int decode(final byte[] qp) {
        final int qplen = qp.length;
        int retlen = 0;

        for (int i = 0; i < qplen; i++) {
            // Handle encoded chars
            if (qp[i] == '=')
                if (qplen - i > 2)
                    // The sequence can be complete, check it
                    if (qp[i + 1] == QuotedPrintableDecoder.CR
                            && qp[i + 2] == QuotedPrintableDecoder.LF) {
                        // soft line break, ignore it
                        i += 2;
                        continue;

                    } else if (QuotedPrintableDecoder.isHexDigit(qp[i + 1])
                            && QuotedPrintableDecoder.isHexDigit(qp[i + 2])) {
                        // convert the number into an integer, taking
                        // the ascii digits stored in the array.
                        qp[retlen++] = (byte) (QuotedPrintableDecoder
                                .getHexValue(qp[i + 1]) * 16 + QuotedPrintableDecoder
                                .getHexValue(qp[i + 2]));

                        i += 2;
                        continue;

                    } else
                        //  Log.error("decode: Invalid sequence = " + qp[i + 1]
                        //    + qp[i + 2]);

                        // RFC 2045 says to exclude control characters mistakenly
                        // present (unencoded) in the encoded stream.
                        // As an exception, we keep unencoded tabs (0x09)
                        if ((qp[i] >= 0x20 && qp[i] <= 0x7f) || qp[i] == QuotedPrintableDecoder.HT
                                || qp[i] == QuotedPrintableDecoder.CR
                                || qp[i] == QuotedPrintableDecoder.LF)
                            qp[retlen++] = qp[i];
        }

        return retlen;
    }

    private static boolean isHexDigit(final byte b) {
        return ((b >= 0x30 && b <= 0x39) || (b >= 0x41 && b <= 0x46));
    }

    private static byte getHexValue(final byte b) {
        return (byte) Character.digit((char) b, 16);
    }

    public static String decode(final byte[] qp, final String enc) {
        final int len = QuotedPrintableDecoder.decode(qp);
        try {
            return new String(qp, 0, len, enc);
        } catch (final UnsupportedEncodingException e) {
            //  Log.error("qp.decode: " + enc + " not supported. " + e.toString());
            return new String(qp, 0, len);
        }
    }


}