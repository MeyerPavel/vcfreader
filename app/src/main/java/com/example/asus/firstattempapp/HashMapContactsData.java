package com.example.asus.firstattempapp;

import android.content.ContentProviderOperation;
import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by ASUS on 10.04.2018.
 */

class HashMapContactsData {

    private String filePath;

    private ArrayList<String> ArrListPhoneNumbers;
    private String prefNumber;
    private StringBuilder Line;
    private String contactName;
    private HashMap<String, ArrayList<String>> hmap_ContactsData;
    private HashMap<String,String> hmap_PrefNumbers;


    HashMapContactsData(String _filePath){
        filePath = _filePath;
    }

    public void assign_HashMapLink(HashMap<String,ArrayList<String>> hmap,HashMap<String,String> hmap_pref) throws FileNotFoundException {
        fillHashMapFromFile();
        hmap.putAll(hmap_ContactsData);
        hmap_pref.putAll(hmap_PrefNumbers);
    }


    private void fillHashMapFromFile() throws FileNotFoundException {
        File file = new File(filePath);//Meyer.vcf
            try (Scanner scanner = new Scanner(file)) {
                hmap_ContactsData = new HashMap<>();
                ArrListPhoneNumbers = new ArrayList<>();
                hmap_PrefNumbers = new HashMap<>();
                StringBuilder subStr;
                String lineAfterFN;
                prefNumber = "";

                while (scanner.hasNextLine()) {
                    Line = new StringBuilder(scanner.nextLine());
                    try {
                        subStr = new StringBuilder(KeyWordSearch());
                        if (subStr.toString().equals("FN")) {
                            hmap_ContactsData.put(contactName,ArrListPhoneNumbers);
                            if(!prefNumber.equals("")){
                                hmap_PrefNumbers.put(contactName,prefNumber);
                                prefNumber = "";
                            }
                            ArrListPhoneNumbers = new ArrayList<>();
                            lineAfterFN = scanner.nextLine();
                            printToFileByKeywords(subStr.toString(), lineAfterFN);
                        } else {
                            printToFileByKeywords(subStr.toString());
                        }

                    } catch (Exception ignored) {

                    }
                }
            }         //  Toast.makeText(this, "Приложение закончило работу", Toast.LENGTH_LONG).show();
    }


    private String KeyWordSearch() {
        int indexOfSemicolon = Line.indexOf(";");
        int indexOfColon = Line.indexOf(":");
        if ((indexOfSemicolon < indexOfColon) && indexOfSemicolon > 0) {
            return Line.substring(0, indexOfSemicolon);
        } else if ((indexOfSemicolon > indexOfColon) && indexOfColon > 0) {
            return Line.substring(0, indexOfColon);
        } else if (indexOfColon < 0 && indexOfSemicolon > 0) {
            return Line.substring(0, indexOfSemicolon);
        } else if (indexOfSemicolon < 0 && indexOfColon > 0) {
            return Line.substring(0, indexOfColon);
        } else {
            return "";
        }
    }

    private void printToFileByKeywords(String str) throws IOException {
        int indexOfColon = 0;
        String Contact_Number;
        if (str.equals("TEL")) {
            indexOfColon = Line.indexOf(":");
            if (isPrefTel(Line.toString())) {
                // printToFile.write(str + ">" + "PREF>" + Line.substring(indexOfColon + 1, Line.length()).replace(";", " ") + "\r\n");
                Contact_Number = Line.substring(indexOfColon + 1, Line.length()).replace(";", " ");
                prefNumber = Contact_Number;
            } else {
                //  printToFile.write(str + ">" + Line.substring(indexOfColon + 1, Line.length()).replace(";", " ") + "\r\n");
                Contact_Number = Line.substring(indexOfColon + 1, Line.length()).replace(";", " ");
                ArrListPhoneNumbers.add(Contact_Number);
            }
        }

    }

    private void printToFileByKeywords( String str, String strLine) throws IOException {
        int indexOfColon = Line.indexOf(":");
        byte[] arrb;
        String Contact_Name;
        String Contact_Number;
        if (isContinuationOfFN(strLine)) {
            String temp = Line.substring(indexOfColon + 1, Line.length() - 1) + strLine;
            arrb = temp.getBytes(Charset.forName("UTF-8"));
            contactName =  QuotedPrintableDecoder.decode(arrb, temp);
        } else {
            String temp = Line.substring(indexOfColon + 1, Line.length());
            arrb = temp.getBytes(Charset.forName("UTF-8"));
            if (!Line.toString().contains("=")) {
                contactName = Line.substring(indexOfColon + 1, Line.length()).replace(";", " ");
            }
            else
                contactName = QuotedPrintableDecoder.decode(arrb, temp);
        }
        indexOfColon = strLine.indexOf(":");
        if (!isContinuationOfFN(strLine)) {
            if (isPrefTel(strLine)) {
                Contact_Number = strLine.substring(indexOfColon + 1, strLine.length()).replace(";", " ");
                prefNumber = Contact_Number;
            } else {
                Contact_Number = strLine.substring(indexOfColon + 1, strLine.length()).replace(";", " ");
                ArrListPhoneNumbers.add(Contact_Number);
            }
        }
    }

    private boolean isContinuationOfFN(String str) {
        for (String splitStr : str.split("[;:/]")) {
            if (splitStr.equals("TEL"))
                return false;
        }
        return true;

    }
    private boolean isPrefTel(String str) {
        for (String splitStr : str.split("[;:]")) {
            if (splitStr.equals("PREF"))
                return true;
        }
        return false;
    }
}
