package com.example.asus.firstattempapp;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ASUS on 31.05.2018.
 */

public class ListViewFragment extends ListFragment {

    ArrayList<String> fileNames;
    HashMap<String,String> hmapFileNameAndPath;
    private FileNameArrayAdapter mFileNameArrayAdapter;
    public void setData(ArrayList<String> _data ,HashMap<String,String> hmap){
        hmapFileNameAndPath = hmap;
        fileNames = _data;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFileNameArrayAdapter = new FileNameArrayAdapter(getActivity(),R.layout.text_image_forlistview,fileNames);
            setListAdapter(mFileNameArrayAdapter);
        return  inflater.inflate(R.layout.listview_fragment, null);
    }
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        String pathToFile = hmapFileNameAndPath.get(fileNames.get(position));
        Log.i("path to file", " ===================" + pathToFile);
        Intent intent = new Intent(getActivity(), MenuActivity.class);
        intent.putExtra("FileName",fileNames.get(position));
        intent.putExtra("FilePath",pathToFile);
        startActivity(intent);

    }
}
